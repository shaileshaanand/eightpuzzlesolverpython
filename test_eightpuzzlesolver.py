import unittest
import eightpuzzlesolver
'''
this script tests the eightpuzzlesolver.py script
'''
class TestEightPuzzleState(unittest.TestCase):
    state1=eightpuzzlesolver.EightPuzzleState([5, 1, 2, 8, 0, 7, 3, 6, 4])
    state2=eightpuzzlesolver.EightPuzzleState([1, 2, 3, 4, 5, 6, 7, 8, 0],'u',13,state1)
    state3=eightpuzzlesolver.EightPuzzleState([7, 1, 5, 3, 2, 8, 0, 4, 6])
    state4=eightpuzzlesolver.EightPuzzleState([5, 1, 2, 8, 0, 7, 3, 6, 4],'l',23,state3)
    def test_all_functions(self):
        #print(self.state1.get_causing_action())
        self.assertEqual(self.state1.get_causing_action(),'n')
        self.assertEqual(self.state2.get_causing_action(),'u')
        self.assertEqual(self.state3.get_causing_action(),'n')
        self.assertEqual(self.state4.get_causing_action(),'l')
        self.assertEqual(self.state1.get_depth(),0)
        self.assertEqual(self.state2.get_depth(),13)
        self.assertEqual(self.state3.get_depth(),0)
        self.assertEqual(self.state4.get_depth(),23)
if __name__ == '__main__':
    unittest.main()
