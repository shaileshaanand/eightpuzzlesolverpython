'''
This is a python script to solve the 8 puzzle
'''
import sys
import heapq
class EightPuzzleState():
    '''
    the EightPuzzleState class represents a game state of the 8 puzzle game
    '''
    def __init__(self, arrangement, causing_action='n', depth=0, parent='none'):
        self.arrangement = list(arrangement)
        self.number = 10 ** 9
        for i in range(len(arrangement)):
            self.number += int(arrangement[i]) * 10 ** i
        self.causing_action = causing_action
        self.depth = depth
        self.parent = parent
        self.score = self.get_a_star_score()

    def get_causing_action(self):
        '''
        this method is a getter for causing_action variable
        '''
        return self.causing_action

    def get_depth(self):
        '''
        this method is a getter for depth variable
        '''
        return self.depth

    def get_parent(self):
        '''
        this method is a getter for parent variable
        '''
        return self.parent

    def get_arrangement(self):
        '''
        this method is a getter for arrangement variable
        '''
        return list(self.arrangement)

    def is_goal_state(self):
        '''
        this method checks if this game state is the goal state of the game
        '''
        return self.arrangement == ['1', '2', '3', '4', '5', '6', '7', '8', '0']

    def get_neighbours(self):
        '''
        this method returns a list of all the  neighbours of this game state as
        EightPuzzleState objects
        '''
        ind = self.arrangement.index('0')
        neighbours = list()
        if ind in (0, 1, 2, 3, 4, 5):
            upneighbour = list(self.arrangement)
            temp = upneighbour[ind]
            upneighbour[ind] = upneighbour[ind+3]
            upneighbour[ind+3] = temp
            neighbours.append(EightPuzzleState(upneighbour, 'u', self.depth+1, self))
        if ind in (0, 1, 3, 4, 6, 7):
            leftneighbour = list(self.arrangement)
            temp = leftneighbour[ind]
            leftneighbour[ind] = leftneighbour[ind+1]
            leftneighbour[ind+1] = temp
            neighbours.append(EightPuzzleState(leftneighbour, 'l', self.depth+1, self))
        if ind in (3, 4, 5, 6, 7, 8):
            downneighbour = list(self.arrangement)
            temp = downneighbour[ind]
            downneighbour[ind] = downneighbour[ind-3]
            downneighbour[ind-3] = temp
            neighbours.append(EightPuzzleState(downneighbour, 'd', self.depth+1, self))
        if ind in (1, 2, 4, 5, 7, 8):
            rightneighbour = list(self.arrangement)
            temp = rightneighbour[ind]
            rightneighbour[ind] = rightneighbour[ind-1]
            rightneighbour[ind-1] = temp
            neighbours.append(EightPuzzleState(rightneighbour, 'r', self.depth+1, self))
        return neighbours

    def get_a_star_score(self):
        '''
        this method calculates the A* Score of this game state by adding depth
        as cost to the heuristic which is calculated by manhattan algoritham
        '''
        score = self.depth
        for i in range(9):
            if int(self.arrangement[i]) > 0:
                difference = abs(int(self.arrangement[i])-i-1)
                initial_tile = min(int(self.arrangement[i]), i)
            else:
                continue
            if difference == 1:
                if initial_tile in (3, 6):
                    score += 3
                else:
                    score += 1
            elif difference == 2:
                score += 2
            elif difference == 3:
                score += 1
            elif difference == 4:
                if initial_tile == 3:
                    score += 4
                else:
                    score += 2
            elif difference == 5:
                score += 3
            elif difference == 6:
                score += 2
            elif difference == 7:
                score += 3
            elif difference == 8:
                score += 4
        print(score)
        return score

    def __eq__(self, other):
        if other == 'none':
            return False
        return self.number == other.number

    def __lt__(self,other):
        return self.score < other.score

if __name__ == '__main__':
    initial_state = EightPuzzleState([letter for letter in sys.argv[1]])
    frontier = list()
    # frontier_no = list()
    heapq.heappush(frontier, initial_state)
    # frontier_no.append()
    explored = set()
    explored_nodes = 0
    while len(frontier) != 0:
        # frontier.sort(key=lambda eps: eps.get_a_star_score())
        current_state = heapq.heappop(frontier)
        # frontier.heappop(0)
        explored.add(current_state.number)
        explored_nodes += 1
        #print(explored_nodes, ' nodes explored    \r')
        if current_state.is_goal_state():
            print(explored_nodes , 'nodes explored    ')
            break
        neighbourslist = current_state.get_neighbours()
        for neighbour in neighbourslist:
            if not((neighbour.number in explored) or (neighbour in frontier)):
                heapq.heappush(frontier, neighbour)
    if current_state.is_goal_state():
        print('Search Depth=',current_state.get_depth())
        steps = ''
        while current_state.get_depth() != 0:
            steps += current_state.get_causing_action()+','
            current_state = current_state.get_parent()
        steps = steps[::-1]
        steps = '{' + steps[1:] + '}'
        print(steps)
    else:
        print('fail')
